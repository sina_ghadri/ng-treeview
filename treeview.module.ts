import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TreeViewComponent } from './components/treeview.component';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
  ],
  declarations: [
    TreeViewComponent
  ],
  exports: [
    TreeViewComponent
  ]
})
export class TreeViewModule { }
