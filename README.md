###

 display items in treeview format.

![alt text](screenshots/01.png)

### Usage
```html
<treeview 
  [service]="Observable<Item[]>" 
  [options]="TreeViewOption" 
  (onSelect)="$evetnt:Item"
></treeview>
```

### Examples

##### *.component.ts
```typescript
@Component({
  selector: 'comp1',
  templateUrl: './*.component.html',
  styleUrls: ['./*.component.css'],
  providers: [Service]
})
export class Component implements OnInit {
  root:Item = new Item();
  
  public constructor(private service: Service) { }
  
  ngOnInit(): void { 
    this.service.get().subscribe(data => this.root.items = data);
  }
  onselctEvent(item:any) {
    /// item.id && item.text : available
  }
}
```
##### *.component.html
```html
<treeview [service]="service.get()" (onSelect)="onselctEvent()"></treeview>
<treeview [root]="root" (onSelect)="onselctEvent()"></treeview>
```
##### *.service.ts
```typescript
@Injectable()
export class Service {

    public constructor(private http: HttpClient) { }

    public get(): Observable<any[]> {
        return this.http.get<any[]>(/*url*/);
    }
 }
```
