import { RequestOptionsArgs } from "@angular/http/src/interfaces";

export class Item {
    public id: number = null;
    public text: string;
    public isOpen: boolean;
    public src: string;
    public items: Item[];
    public parent: Item;
    public parentId: number;

    public constructor(item:Item = null) {
        this.id = (item ? item.id : null);
        this.src = (item ? item.src : null);
        this.text = (item ? item.text : null);
        this.items = (item ? item.items : []);
        this.parent = (item ? item.parent : null);
        this.parentId = (item ? item.parentId : null);
    }
    public isFile() { return this.items.length == 0; }
    public close() {
        for(let i = 0;i < this.items.length;i++) {
            this.items[i].close();
            this.isOpen = false;
        }
    }
    public open() { 
        if(this.parent) 
        {
            this.isOpen = true;
            this.parent.open();
        }
    }
}

export class Options {
    public search: string;
    public requestOptionArgs: RequestOptionsArgs;
}