import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Item, Options } from "../models";
import { Observable } from "rxjs/Observable";

@Component({
    selector: 'treeview',
    templateUrl: './treeview.component.html',
    styleUrls: ['./treeview.component.css'],
})
export class TreeViewComponent implements OnInit {

    @Input() root: Item = new Item;
    @Input() options: Options = new Options;
    @Input() service: Observable<Item[]>;

    @Output() onSelect: EventEmitter<Item> = new EventEmitter;

    ngOnInit(): void { this.reload(); }
    reload() {
        if (this.service) this.service.subscribe(data => this.optimize(this.root, data));
    }

    public optimize(item: Item, items: Item[]) {
        item.items = [];
        for (let i = 0; i < items.length; i++) {
            if (items[i].parentId === item.id) {
                this.optimize(items[i], items);
                items[i].parent = item;
                item.items.push(new Item(items[i]));
            }
        }
    }
    public search(item: Item): boolean {
        let search = this.options.search.toLowerCase();
        let flag = false;
        for (let i = 0; i < item.items.length; i++) {
            if (item.items[i].isFile()) {
                if (item.items[i].text.toLowerCase().indexOf(search) > -1) return true;
            } else {
                item.items[i].isOpen = this.search(item.items[i]);
                if (item.items[i].isOpen) flag = true;
            }
        }
        return flag;
    }

    public toggle(item: Item) { item.isOpen = (item.isOpen ? false : true); }
    public getItems(): Item[] {
        if (this.root.id == null && this.options.search) {
            this.root.close();
            this.search(this.root);
        }
        return this.root.items;
    }
    public select(item: Item) { this.onSelect.emit(item); }
}